import unittest
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, LongType, StringType, ArrayType
from pyspark.sql.functions import cast
from pysparkfinaltask.spark.rdd_impl import (
    calculate_awards_count_rdd,
    calculate_goals_count_rdd,
    get_distinct_teams_rdd,
    full_outer_join_rdd,
    get_player_teams_rdd,
    join_awards_goals_and_teams,
    create_player_name_rdd,
    join_and_select_full_info_rdd,
    get_top_10_players_by_goals
)


class TestRDDImpl(unittest.TestCase):
    spark = None

    @classmethod
    def setUpClass(cls):
        # Set up a Spark session for the tests
        cls.spark = SparkSession.builder.master("local[2]").appName("test").getOrCreate()
        cls.spark.sparkContext.setLogLevel("FATAL")

    @classmethod
    def tearDownClass(cls):
        if hasattr(cls, 'spark') and cls.spark is not None:
            cls.spark.stop()

    def test_calculate_awards_count(self):
        schema = StructType([StructField('playerID', StringType(), True),
                             StructField('Awards', LongType(), True)])
        awards_data = [("player1", 1), ("player1", 1), ("player2", 1), ("player2", 1), ("player2", 1)]
        awards_df = self.spark.createDataFrame(awards_data, schema=schema)
        expected_data = [("player1", 2), ("player2", 3)]
        expected_df = self.spark.createDataFrame(expected_data, schema=schema)
        result_df = calculate_awards_count_rdd(awards_df.rdd).toDF(schema=schema)
        self.assertDataFrameEqual(result_df, expected_df)

    def test_calculate_goals_count(self):
        schema = StructType([StructField('playerID', StringType(), True),
                             StructField('Goals', LongType(), False)])
        goals_data = [("player1", 1), ("player1", 1), ("player2", 1), ("player2", 1), ("player2", 1)]
        goals_rdd = self.spark.sparkContext.parallelize(goals_data)
        expected_data = [("player1", 2), ("player2", 3)]
        expected_df = self.spark.createDataFrame(expected_data, schema=schema)
        result_df = calculate_goals_count_rdd(goals_rdd).toDF(schema=schema)
        self.assertDataFrameEqual(result_df, expected_df)

    def test_get_distinct_teams(self):
        schema_expected = StructType([StructField('tmID', StringType(), False),
                                      StructField('name', StringType(), False)])
        team_data = [("", "", "team1", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "a"),
                     ("", "", "team1", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "a"),
                     ("", "", "team2", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "b"),
                     ("", "", "team3", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "c"),
                     ("", "", "team2", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "b")]
        team_rdd = self.spark.sparkContext.parallelize(team_data)
        expected_data = [("team1", "a"), ("team2", "b"), ("team3", "c")]
        expected_df = self.spark.createDataFrame(expected_data, schema=schema_expected)
        result_df = get_distinct_teams_rdd(team_rdd).toDF(schema=schema_expected)
        self.assertDataFrameEqual(result_df, expected_df)

    def test_join_awards_and_goals(self):
        schema_expected = StructType([StructField('playerID', StringType(), True),
                                      StructField('Awards', LongType(), True),
                                      StructField('Goals', LongType(), True)])
        awards_data = [("player1", 1), ("player2", 2), ("player3", None), ("player4", None), ("player5", 1)]
        goals_data = [("player1", 5), ("player2", 8), ("player3", 2), ("player4", None), ("player5", None)]
        goals_rdd = self.spark.sparkContext.parallelize(goals_data)
        awards_rdd = self.spark.sparkContext.parallelize(awards_data)
        expected_data = [("player1", 1, 5), ("player2", 2, 8), ("player3", None, 2), ("player4", None, None),
                         ("player5", 1, None)]
        expected_df = self.spark.createDataFrame(expected_data, schema=schema_expected).sort("playerID")
        result_df = (full_outer_join_rdd(awards_rdd, goals_rdd)
                     .map(lambda x: (x[0], x[1][0], x[1][1]))
                     .toDF(schema=schema_expected).sort("playerID"))
        self.assertDataFrameEqual(result_df, expected_df)

    def test_get_player_teams(self):
        schema_expected = StructType([StructField('playerID', StringType(), True),
                                      StructField('Teams', ArrayType(StringType(), False), False)])
        team_data = [("team1", "a-team"), ("team2", "b-team"), ("team3", "c-team")]
        scoring_data = [("player1", None, None, "team1"), ("player1", None, None, "team2"),
                        ("player2", None, None, "team3"),
                        ("player2", None, None, "team1"),
                        ("player3", None, None, "team3"), ("player3", None, None, "team3")]

        team_rdd = self.spark.sparkContext.parallelize(team_data)
        scoring_rdd = self.spark.sparkContext.parallelize(scoring_data)
        expected_data = [("player3", ["c-team"]), ("player1", ["b-team", "a-team"]), ("player2", ["c-team", "a-team"])]
        expected_df = self.spark.createDataFrame(expected_data, schema=schema_expected).sort("playerID")
        result_df = get_player_teams_rdd(scoring_rdd, team_rdd).toDF(schema=schema_expected).sort("playerID")
        self.assertDataFrameEqual(expected_df, result_df)

    def test_join_awards_goals_and_teams(self):
        schema_teams = StructType([StructField('playerID', StringType(), True),
                                   StructField('Teams', ArrayType(StringType(), False), False)])

        schema_expected = StructType([StructField('playerID', StringType(), True),
                                      StructField('Awards', LongType(), True),
                                      StructField('Goals', LongType(), True),
                                      StructField('Teams', ArrayType(StringType(), False), True)])
        teams_data = [("player3", ["c-team"]), ("player1", ["a-team", "b-team"]), ("player2", ["a-team", "c-team"])]
        awards_goals_data = [("player1", (1, 5)), ("player2", (2, 8)), ("player3", (None, 2)),
                             ("player4", (None, None)),
                             ("player5", (1, None))]
        expected_data = [("player1", 1, 5, ["a-team", "b-team"]), ("player2", 2, 8, ["a-team", "c-team"]),
                         ("player3", None, 2, ["c-team"]), ("player4", None, None, None),
                         ("player5", 1, None, None)]
        awards_goals_rdd = self.spark.sparkContext.parallelize(awards_goals_data)
        expected_df = self.spark.createDataFrame(expected_data, schema=schema_expected)
        teams_df = self.spark.createDataFrame(teams_data, schema=schema_teams)
        result_df = (join_awards_goals_and_teams(awards_goals_rdd, teams_df.rdd)
                     .map(lambda x: (x[0], cast(LongType, x[1][0][0]), cast(LongType, x[1][0][1]), x[1][1]))
                     .toDF(schema=schema_expected)
                     .sort("playerID"))
        self.assertDataFrameEqual(result_df, expected_df)

    def test_create_player_name_rdd(self):
        schema_expected = StructType([StructField('playerID', StringType(), True),
                                      StructField('Name', StringType(), False)])
        master_data = [("player1", "", "", "john", "wick"),
                       ("player2", "", "", "Max", "Pain"),
                       ("player3", "", "", "Neo", "neo")]
        expected_data = [("player1", "john wick"), ("player2", "Max Pain"), ("player3", "Neo neo")]
        expected_df = self.spark.createDataFrame(expected_data, schema_expected)
        master_rdd = self.spark.sparkContext.parallelize(master_data)
        result_df = (create_player_name_rdd(master_rdd)
                     .toDF(schema=schema_expected)
                     .sort("playerID"))
        self.assertDataFrameEqual(result_df, expected_df)

    def test_join_and_select_full_info(self):
        schema_expected = StructType([StructField('Name', StringType(), True),
                                      StructField('Awards', LongType(), True),
                                      StructField('Goals', LongType(), True),
                                      StructField('Teams', ArrayType(StringType(), False), True)])
        data_expected = [("john wick", 1, 5, ["a-team", "b-team"]), ("Max Pain", 2, 8, ["a-team", "c-team"]),
                         ("Neo neo", None, 2, ["c-team"])]
        expected_df = self.spark.createDataFrame(data_expected, schema=schema_expected).sort("Name", ascending=True)
        full_data = [("player1", ((1, 5), ["a-team", "b-team"])), ("player2", ((2, 8), ["a-team", "c-team"])),
                     ("player3", ((None, 2), ["c-team"]))]
        name_data = [("player1", "john wick"), ("player2", "Max Pain"), ("player3", "Neo neo")]
        full_rdd = self.spark.sparkContext.parallelize(full_data)
        name_rdd = self.spark.sparkContext.parallelize(name_data)
        result_df = (join_and_select_full_info_rdd(full_rdd, name_rdd)
                     .toDF(schema=schema_expected)
                     .sort("Name", ascending=True))
        self.assertDataFrameEqual(result_df, expected_df)

    def test_get_top_10_players_by_goals(self):
        schema = StructType([StructField('Name', StringType(), True),
                             StructField('Awards', LongType(), True),
                             StructField('Goals', LongType(), True),
                             StructField('Teams', ArrayType(StringType(), False), True)])
        data_input = [("john wick", 1, 5, ["a-team", "b-team"]),
                      ("Max Pain", 2, 8, ["a-team", "c-team"]),
                      ("Neo neo", None, 3, ["c-team"]),
                      ("john wick1", 1, 15, ["a-team", "b-team"]),
                      ("Max Pain1", 2, 13, ["a-team", "c-team"]),
                      ("Neo neo1", None, 2, ["c-team"]),
                      ("john wick3_not expected", 1, 1, ["a-team", "b-team"]),
                      ("Max Pain2", 2, 8, ["a-team", "c-team"]),
                      ("Neo neo_not expected", None, 1, ["c-team"]),
                      ("john wick2", 1, 6, ["a-team", "b-team"]),
                      ("Max Pain3", 2, 6, ["a-team", "c-team"]),
                      ("Neo neo3", None, 2, ["c-team"])]
        data_expected = [("john wick1", 1, 15, ["a-team", "b-team"]),
                         ("Max Pain1", 2, 13, ["a-team", "c-team"]),
                         ("Max Pain", 2, 8, ["a-team", "c-team"]),
                         ("Max Pain2", 2, 8, ["a-team", "c-team"]),
                         ("john wick2", 1, 6, ["a-team", "b-team"]),
                         ("Max Pain3", 2, 6, ["a-team", "c-team"]),
                         ("john wick", 1, 5, ["a-team", "b-team"]),
                         ("Neo neo", None, 3, ["c-team"]),
                         ("Neo neo1", None, 2, ["c-team"]),
                         ("Neo neo3", None, 2, ["c-team"])]

        input_rdd = self.spark.sparkContext.parallelize(data_input)
        expected_df = self.spark.createDataFrame(data_expected, schema)
        result_df = get_top_10_players_by_goals(input_rdd).toDF(schema).limit(10)
        self.assertDataFrameEqual(result_df, expected_df)

    def assertDataFrameEqual(self, df1, df2):
        self.assertEqual(df1.schema, df2.schema)
        self.assertEqual(df1.collect(), df2.collect())


if __name__ == "__main__":
    unittest.main()
