import unittest
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, LongType, StringType, ArrayType
from pysparkfinaltask.spark.dataframe_impl import (
    calculate_awards_count,
    calculate_goals_count,
    get_distinct_teams,
    join_awards_and_goals,
    get_player_teams,
    join_awards_goals_and_teams,
    create_player_name_df,
    join_and_select_full_info,
    get_top_10_players_by_goals
)


class TestDataframeImpl(unittest.TestCase):
    spark = None

    @classmethod
    def setUpClass(cls):
        # Set up a Spark session for the tests
        cls.spark = SparkSession.builder.master("local[2]").appName("test").getOrCreate()
        cls.spark.sparkContext.setLogLevel("FATAL")

    @classmethod
    def tearDownClass(cls):
        if hasattr(cls, 'spark') and cls.spark is not None:
            cls.spark.stop()

    def test_calculate_awards_count(self):
        schema = StructType([StructField('playerID', StringType(), True),
                             StructField('Awards', LongType(), False)])
        awards_data = [("player1", 1), ("player1", 1), ("player2", 1), ("player2", 1), ("player2", 1)]
        awards_df = self.spark.createDataFrame(awards_data, schema=schema)
        expected_data = [("player1", 2), ("player2", 3)]
        expected_df = self.spark.createDataFrame(expected_data, schema=schema)
        result_df = calculate_awards_count(awards_df)
        self.assertDataFrameEqual(result_df, expected_df)

    def test_calculate_goals_count(self):
        schema = StructType([StructField('playerID', StringType(), True),
                             StructField('Goals', LongType(), False)])
        goals_data = [("player1", 1), ("player1", 1), ("player2", 1), ("player2", 1), ("player2", 1)]
        goals_df = self.spark.createDataFrame(goals_data, schema=schema)
        expected_data = [("player1", 2), ("player2", 3)]
        expected_df = self.spark.createDataFrame(expected_data, schema=schema)
        result_df = calculate_goals_count(goals_df)
        self.assertDataFrameEqual(result_df, expected_df)

    def test_get_distinct_teams(self):
        schema = StructType([StructField('tmID', StringType(), False),
                             StructField('name', StringType(), False)])
        team_data = [("team1", "a"), ("team1", "a"), ("team2", "b"), ("team3", "c"), ("team2", "b")]
        team_df = self.spark.createDataFrame(team_data, schema=schema)
        expected_data = [("team1", "a"), ("team2", "b"), ("team3", "c")]
        expected_df = self.spark.createDataFrame(expected_data, schema=schema)
        result_df = get_distinct_teams(team_df)
        self.assertDataFrameEqual(result_df, expected_df)

    def test_join_awards_and_goals(self):
        schema_goals = StructType([StructField('playerID', StringType(), False),
                                   StructField('Goals', LongType(), True)])
        schema_awards = StructType([StructField('playerID', StringType(), False),
                                    StructField('Awards', LongType(), True)])
        schema_expected = StructType([StructField('playerID', StringType(), True),
                                      StructField('Awards', LongType(), True),
                                      StructField('Goals', LongType(), True)])
        awards_data = [("player1", 1), ("player2", 2), ("player3", None), ("player4", None), ("player5", 1)]
        goals_data = [("player1", 5), ("player2", 8), ("player3", 2), ("player4", None), ("player5", None)]
        goals_df = self.spark.createDataFrame(goals_data, schema=schema_goals)
        awards_df = self.spark.createDataFrame(awards_data, schema=schema_awards)
        expected_data = [("player1", 1, 5), ("player2", 2, 8), ("player3", None, 2), ("player4", None, None),
                         ("player5", 1, None)]
        expected_df = self.spark.createDataFrame(expected_data, schema=schema_expected)
        result_df = join_awards_and_goals(awards_df, goals_df)
        self.assertDataFrameEqual(result_df, expected_df)

    def test_get_player_teams(self):
        schema_teams = StructType([StructField('tmID', StringType(), True),
                                   StructField('name', StringType(), True)])
        schema_scoring = StructType([StructField('playerID', StringType(), True),
                                     StructField('tmID', StringType(), True)])
        schema_expected = StructType([StructField('playerID', StringType(), True),
                                      StructField('Teams', ArrayType(StringType(), False), False)])
        team_data = [("team1", "a-team"), ("team2", "b-team"), ("team3", "c-team")]
        scoring_data = [("player1", "team1"), ("player1", "team2"), ("player2", "team3"), ("player2", "team1"),
                        ("player3", "team3"), ("player3", "team3")]
        team_df = self.spark.createDataFrame(team_data, schema=schema_teams)
        scoring_df = self.spark.createDataFrame(scoring_data, schema=schema_scoring)
        expected_data = [("player3", ["c-team"]), ("player1", ["a-team", "b-team"]), ("player2", ["a-team", "c-team"])]
        expected_df = self.spark.createDataFrame(expected_data, schema=schema_expected)
        result_df = get_player_teams(scoring_df, team_df)
        self.assertDataFrameEqual(result_df, expected_df)

    def test_join_awards_goals_and_teams(self):
        schema_teams = StructType([StructField('playerID', StringType(), True),
                                   StructField('Teams', ArrayType(StringType(), False), False)])
        schema_awards_goals = StructType([StructField('playerID', StringType(), True),
                                          StructField('Awards', LongType(), True),
                                          StructField('Goals', LongType(), True)])
        schema_expected = StructType([StructField('playerID', StringType(), True),
                                      StructField('Awards', LongType(), True),
                                      StructField('Goals', LongType(), True),
                                      StructField('Teams', ArrayType(StringType(), False), True)])
        teams_data = [("player3", ["c-team"]), ("player1", ["a-team", "b-team"]), ("player2", ["a-team", "c-team"])]
        awards_goals_data = [("player1", 1, 5), ("player2", 2, 8), ("player3", None, 2), ("player4", None, None),
                             ("player5", 1, None)]
        expected_data = [("player1", 1, 5, ["a-team", "b-team"]), ("player2", 2, 8, ["a-team", "c-team"]),
                         ("player3", None, 2, ["c-team"]), ("player4", None, None, None),
                         ("player5", 1, None, None)]
        expected_df = self.spark.createDataFrame(expected_data, schema=schema_expected)
        awards_goals_df = self.spark.createDataFrame(awards_goals_data, schema=schema_awards_goals)
        teams_df = self.spark.createDataFrame(teams_data, schema=schema_teams)
        result_df = join_awards_goals_and_teams(awards_goals_df, teams_df)
        self.assertDataFrameEqual(result_df, expected_df)

    def test_create_player_name_df(self):
        schema_master = StructType([StructField('playerID', StringType(), True),
                                    StructField('firstName', StringType(), True),
                                    StructField('lastName', StringType(), True)])
        schema_expected = StructType([StructField('playerID', StringType(), True),
                                      StructField('Name', StringType(), False)])
        master_data = [("player1", "john", "wick"), ("player2", "Max", "Pain"), ("player3", "Neo", "neo")]
        expected_data = [("player1", "john wick"), ("player2", "Max Pain"), ("player3", "Neo neo")]
        master_df = self.spark.createDataFrame(master_data, schema=schema_master)
        expected_df = self.spark.createDataFrame(expected_data, schema=schema_expected)
        result_df = create_player_name_df(master_df).sort("playerID")
        self.assertDataFrameEqual(result_df, expected_df)

    def test_join_and_select_full_info(self):
        schema_name = StructType([StructField('playerID', StringType(), True),
                                  StructField('Name', StringType(), False)])
        name_data = [("player1", "john wick"), ("player2", "Max Pain"), ("player3", "Neo neo")]
        schema_full = StructType([StructField('playerID', StringType(), True),
                                  StructField('Awards', LongType(), True),
                                  StructField('Goals', LongType(), True),
                                  StructField('Teams', ArrayType(StringType(), False), True)])
        full_data = [("player1", 1, 5, ["a-team", "b-team"]), ("player2", 2, 8, ["a-team", "c-team"]),
                     ("player3", None, 2, ["c-team"])]
        schema_expected = StructType([StructField('Name', StringType(), True),
                                      StructField('Awards', LongType(), True),
                                      StructField('Goals', LongType(), True),
                                      StructField('Teams', ArrayType(StringType(), False), True)])
        data_expected = [("john wick", 1, 5, ["a-team", "b-team"]), ("Max Pain", 2, 8, ["a-team", "c-team"]),
                         ("Neo neo", None, 2, ["c-team"])]
        name_df = self.spark.createDataFrame(name_data, schema=schema_name)
        full_df = self.spark.createDataFrame(full_data, schema=schema_full)
        expected_df = self.spark.createDataFrame(data_expected, schema=schema_expected).sort("Name", ascending=True)
        result_df = join_and_select_full_info(full_df, name_df).sort("Name", ascending=True)
        self.assertDataFrameEqual(result_df, expected_df)

    def test_get_top_10_players_by_goals(self):
        schema = StructType([StructField('Name', StringType(), True),
                             StructField('Awards', LongType(), True),
                             StructField('Goals', LongType(), True),
                             StructField('Teams', ArrayType(StringType(), False), True)])
        data_input = [("john wick", 1, 5, ["a-team", "b-team"]),
                      ("Max Pain", 2, 8, ["a-team", "c-team"]),
                      ("Neo neo", None, 3, ["c-team"]),
                      ("john wick1", 1, 15, ["a-team", "b-team"]),
                      ("Max Pain1", 2, 13, ["a-team", "c-team"]),
                      ("Neo neo1", None, 2, ["c-team"]),
                      ("john wick3_not expected", 1, 1, ["a-team", "b-team"]),
                      ("Max Pain2", 2, 8, ["a-team", "c-team"]),
                      ("Neo neo_not expected", None, 1, ["c-team"]),
                      ("john wick2", 1, 6, ["a-team", "b-team"]),
                      ("Max Pain3", 2, 6, ["a-team", "c-team"]),
                      ("Neo neo3", None, 2, ["c-team"])]
        data_expected = [("john wick1", 1, 15, ["a-team", "b-team"]),
                         ("Max Pain1", 2, 13, ["a-team", "c-team"]),
                         ("Max Pain2", 2, 8, ["a-team", "c-team"]),
                         ("Max Pain", 2, 8, ["a-team", "c-team"]),
                         ("john wick2", 1, 6, ["a-team", "b-team"]),
                         ("Max Pain3", 2, 6, ["a-team", "c-team"]),
                         ("john wick", 1, 5, ["a-team", "b-team"]),
                         ("Neo neo", None, 3, ["c-team"]),
                         ("Neo neo3", None, 2, ["c-team"]),
                         ("Neo neo1", None, 2, ["c-team"])]

        input_df = self.spark.createDataFrame(data_input, schema)
        expected_df = self.spark.createDataFrame(data_expected, schema)
        result_df = get_top_10_players_by_goals(input_df)
        self.assertDataFrameEqual(result_df, expected_df)

    def assertDataFrameEqual(self, df1, df2):
        self.assertEqual(df1.schema, df2.schema)
        self.assertEqual(df1.collect(), df2.collect())


if __name__ == "__main__":
    unittest.main()
