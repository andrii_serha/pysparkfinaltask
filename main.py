from utils import spark_factory, file_reader, file_writer
from spark import dataframe_impl, rdd_impl, sql_impl
import logging
from pyspark.errors.exceptions.base import AnalysisException
from py4j.protocol import Py4JJavaError
import os
import sys

if __name__ == '__main__':
    # Add the root directory to the Python path
    spark = spark_factory.SparkSessionSingleton().get_spark_session()
    spark.sparkContext.setLogLevel("FATAL")
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)
    run_method_list = ["Dataframe", "RDD", "SQL"]
    run_method = "Dataframe"

    if len(sys.argv) > 1:
        if sys.argv[1] in run_method_list:
            run_method = sys.argv[1]

    print(run_method)

    try:
        # Load CSV files into DataFrames
        awards_df = file_reader.readCSV(spark, "source/AwardsPlayers.csv")
        scoring_df = file_reader.readCSV(spark, "source/Scoring.csv")
        teams_df = file_reader.readCSV(spark, "source/Teams.csv")
        master_df = file_reader.readCSV(spark, "source/Master.csv")
        result_df = None
        output_path = "sink/dataframe/output_text"
        if run_method == "Dataframe":
            result_df = dataframe_impl.process_data_using_dataframe(awards_df, scoring_df, teams_df, master_df)
        elif run_method == "SQL":
            result_df = sql_impl.process_data_using_dataframe_sql(spark, awards_df, scoring_df, teams_df, master_df)
            output_path = "sink/sql/output_text"
        else:
            result_df = rdd_impl.process_data_using_rdd(spark, awards_df, scoring_df, teams_df, master_df)
            output_path = "sink/rdd/output_text"

        """Save the result in different formats (text, Avro, Parquet)"""
        if result_df is not None:
            file_writer.write_to_text(result_df, output_path=output_path)
            file_writer.write_to_avro(result_df, output_path=output_path)
            file_writer.write_to_parquet(result_df, output_path=output_path)

    except AnalysisException as e:
        logger.info(f"You are facing an issue with spark job. Cause: {str(e)}")

    except Py4JJavaError as e:
        if "java.io.IOException" in str(e.java_exception):
            logger.error("An IOException occurred in PySpark.")
        else:
            logger.error(f"PySpark error: {str(e)}")
    finally:
        spark.stop()
