from setuptools import setup, find_packages

setup(
    name='pysparkfinaltask',
    version='1.0.0',
    packages=find_packages(),
    install_requires=[
        'coverage' == '7.3.2',
        'py4j' == '0.10.9.7',
        'pyspark' == '3.5.0',
        'pytest' == '7.4.3'
    ],
)
