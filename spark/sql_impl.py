def process_data_using_dataframe_sql(spark, awards_df, scoring_df, teams_df, master_df):
    # Register DataFrames as temporary SQL tables
    awards_df.createOrReplaceTempView("awards_table")
    scoring_df.createOrReplaceTempView("scoring_table")
    teams_df.createOrReplaceTempView("teams_table")
    master_df.createOrReplaceTempView("master_table")

    # Count number of awards for each player using SQL
    (spark.sql("SELECT playerID, COUNT(*) AS Awards FROM awards_table GROUP BY playerID")
     .createOrReplaceTempView("awards_count_view"))

    # Count number of goals for each player using SQL
    (spark.sql("SELECT playerID, COUNT(*) AS Goals FROM scoring_table GROUP BY playerID")
     .createOrReplaceTempView("goals_count_view"))

    # Find distinct teams
    (spark.sql("SELECT DISTINCT tmID, name FROM teams_table")
     .createOrReplaceTempView("distinct_team_name_view"))

    # Join awards and goals by playerID
    spark.sql("""
        SELECT 
            CASE
                WHEN (a.playerID is not null) THEN a.playerID
                WHEN (a.playerID is null AND g.playerID is not NULL) THEN g.playerID 
            END as playerID,
            Awards,
            Goals
        FROM awards_count_view a
        FULL OUTER JOIN goals_count_view g 
        ON a.playerID = g.playerID
    """).createOrReplaceTempView("awards_goals_by_player_view")

    # Get teams for each player based on scoring data and distinct team names
    spark.sql("""
        SELECT s.playerID, COLLECT_LIST(t.name) AS Teams
        FROM 
        (SELECT DISTINCT playerID,tmID FROM scoring_table) s
        LEFT JOIN distinct_team_name_view t ON s.tmID = t.tmID
        GROUP BY s.playerID
    """).createOrReplaceTempView("player_teams_view")

    # Join DataFrames representing awards and goals by player and player teams
    spark.sql("""
        SELECT ag.playerID, ag.Awards, ag.Goals, COALESCE(pt.Teams, NULL) AS Teams
        FROM awards_goals_by_player_view ag
        LEFT JOIN player_teams_view pt ON ag.playerID = pt.playerID
    """).createOrReplaceTempView("full_info_by_id_view")

    # Create a DataFrame with player names by concatenating firstName and lastName
    spark.sql("""
        SELECT DISTINCT playerID, CONCAT_WS(' ', firstName, lastName) AS Name
        FROM master_table
    """).createOrReplaceTempView("player_full_name_view")

    # Join DataFrames representing full info by player full names
    spark.sql("""
        SELECT p.Name, f.Awards, f.Goals, f.Teams
        FROM full_info_by_id_view f
        LEFT JOIN player_full_name_view p ON f.playerID = p.playerID
    """).createOrReplaceTempView("full_info_view")

    # Get the top 10 players by goals from the provided DataFrame
    top_10_goals_df = spark.sql("""SELECT * FROM full_info_view ORDER BY Goals DESC LIMIT 10""")
    return top_10_goals_df
