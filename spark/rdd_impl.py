from operator import add
from pyspark import RDD


def process_data_using_rdd(spark, awards_df, scoring_df, teams_df, master_df):
    # Count number of awards for each player using RDD
    awards_count_rdd = calculate_awards_count_rdd(awards_df.rdd)

    # Count number of goals for each player using RDD
    goals_count_rdd = calculate_goals_count_rdd(scoring_df.rdd)

    # Find in which teams each player played using RDD
    distinct_team_name_rdd = get_distinct_teams_rdd(teams_df.rdd)

    # Join awards and goals RDDs
    awards_goals_by_player_rdd = full_outer_join_rdd(awards_count_rdd, goals_count_rdd)

    # Join with teams RDD
    player_teams_rdd = get_player_teams_rdd(scoring_df.rdd, distinct_team_name_rdd)

    # Join awards_goals_by_player and player_teams RDDs
    full_info_by_id_rdd = join_awards_goals_and_teams(awards_goals_by_player_rdd, player_teams_rdd)

    # Create RDD for player full name
    player_full_name_rdd = create_player_name_rdd(master_df.rdd)

    # Join full_info_by_id and player_full_name RDDs and extract from tuple into separate columns
    full_info_rdd = join_and_select_full_info_rdd(full_info_by_id_rdd, player_full_name_rdd)

    # Filter out None values and format the result
    top_10_goals_rdd = get_top_10_players_by_goals(full_info_rdd).take(10)

    top_10_goals_df = spark.createDataFrame(top_10_goals_rdd, ["Name", "Awards", "Goals", "Teams"]).coalesce(1)
    # top_10_goals_df.show()
    return top_10_goals_df


def calculate_awards_count_rdd(awards_rdd: RDD) -> RDD:
    """
        Calculate the count of awards for each player using RDD.

        Parameters:
            awards_rdd (pyspark.RDD): PySpark RDD containing awards data.

        Returns:
            pyspark.RDD: PySpark RDD with the count of awards for each player.
        """
    awards_rdd_count = (awards_rdd.map(lambda x: (x[0], 1))
                        .reduceByKey(lambda a, b: a + b))
    return awards_rdd_count


def calculate_goals_count_rdd(scoring_rdd: RDD) -> RDD:
    """
    Calculate the count of goals for each player using RDD.

    Parameters:
        scoring_rdd (pyspark.RDD): PySpark RDD containing scoring data.

    Returns:
        pyspark.RDD: PySpark RDD with the count of goals for each player.
    """
    goals_count_rdd = scoring_rdd.map(lambda x: (x[0], 1)).reduceByKey(add)
    return goals_count_rdd


def get_distinct_teams_rdd(teams_rdd: RDD) -> RDD:
    """
    Get distinct teams from the input RDD.

    Parameters:
        teams_rdd (pyspark.RDD): PySpark RDD containing teams data.

    Returns:
        pyspark.RDD: PySpark RDD with distinct teams.
    """
    distinct_teams_rdd = teams_rdd.map(lambda x: (x[2], x[18])).distinct()
    return distinct_teams_rdd


def full_outer_join_rdd(awards_rdd, goals_rdd):
    """
    Perform a full outer join on two RDDs.

    Parameters:
        awards_rdd (RDD): PySpark RDD containing awards data.
        goals_rdd (RDD): PySpark RDD containing goals data.

    Returns:
        RDD: PySpark RDD with unpacked data (playerID, awards, goals)
    """
    joined_rdd = awards_rdd.fullOuterJoin(goals_rdd)
    # unpacked_rdd = joined_rdd.map(lambda x: (x[0], x[1][0], x[1][1]))
    return joined_rdd


def get_player_teams_rdd(scoring_rdd, distinct_team_name_rdd):
    """
    Join RDDs representing awards count and distinct teams using a left outer join.

    Parameters:
        scoring_rdd (RDD): PySpark RDD containing awards count data.
        distinct_team_name_rdd (RDD): PySpark RDD containing distinct team names.

    Returns:
        RDD: PySpark RDD with awards count and teams joined on playerID.
    """
    return scoring_rdd.map(lambda x: (x[3], x[0])) \
        .distinct() \
        .leftOuterJoin(distinct_team_name_rdd) \
        .map(lambda x: (x[1][0], [x[1][1]])) \
        .reduceByKey(add)


def join_awards_goals_and_teams(awards_goals_by_player_rdd, player_teams_rdd):
    return awards_goals_by_player_rdd.fullOuterJoin(player_teams_rdd)


def create_player_name_rdd(master_rdd):
    """
    Process the master RDD by filtering out rows with non-null values in the first column,
    and creating a new RDD with a tuple of the first column and a formatted string of the fourth
    and fifth columns.

    Parameters:
        master_rdd (RDD): PySpark RDD containing master data.

    Returns:
        RDD: Resulting PySpark RDD with processed data.
    """
    # Filter out rows with non-null values in the first column
    filtered_rdd = master_rdd.filter(lambda x: x[0] is not None)

    # Create a new RDD with a tuple of the first column and a formatted string of the fourth and fifth columns
    processed_rdd = filtered_rdd.map(lambda x: (x[0], f"{x[3]} {x[4]}")).distinct()

    return processed_rdd


def join_and_select_full_info_rdd(full_info_by_id_rdd, player_full_name_rdd):
    """
    Join RDDs representing full info by player ID and player full names using a left join.
    Select columns "Name", "Awards", "Goals", and "Teams".

    Parameters:
        full_info_by_id_rdd (RDD): PySpark RDD containing full info by player ID.
        player_full_name_rdd (RDD): PySpark RDD containing player full names.

    Returns:
        RDD: Resulting PySpark RDD with selected columns joined on playerID.
    """
    # Join RDDs representing full info by player ID and player full names using a left join
    joined_rdd = (full_info_by_id_rdd.leftOuterJoin(player_full_name_rdd)
                  .map(lambda x: (x[1][1], x[1][0][0][0] if x[1][0][0][0] else None,
                                  x[1][0][0][1] if x[1][0][0][1] else None, x[1][0][1])))

    return joined_rdd


def get_top_10_players_by_goals(full_info_rdd):
    """
    Sorts an RDD in descending order based on a specified column index.

    Parameters:
        full_info_rdd (pyspark.rdd.RDD): Input RDD.

    Returns:
        pyspark.rdd.RDD: Sorted RDD in descending order
    """
    return full_info_rdd.sortBy(lambda x: x[2], ascending=False)
