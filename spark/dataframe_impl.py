from pyspark.sql.functions import col, count, collect_list, concat_ws
from pyspark.sql import DataFrame


def process_data_using_dataframe(awards_df, scoring_df, teams_df, master_df):
    """ Count number of awards for each player using DataFrame API"""
    awards_count_df = calculate_awards_count(awards_df)

    """Count number of goals (Scoring.stint) for each player using DataFrame API"""
    goals_count_df = calculate_goals_count(scoring_df)

    """Find in which teams each player played using DataFrame API"""
    distinct_team_name_df = get_distinct_teams(teams_df)

    """Joining awards and goals for every particular player by playerID. 
    Possible to have NULL value for goals and/or awards """
    awards_goals_by_player_df = join_awards_and_goals(awards_count_df, goals_count_df)

    """Get teams for each player based on scoring data and distinct team names using a left join. """
    player_teams_df = get_player_teams(scoring_df, distinct_team_name_df)

    """Join DataFrames representing awards and goals by player and player teams using a left join."""
    full_info_by_id_df = join_awards_goals_and_teams(awards_goals_by_player_df, player_teams_df)

    """Create a DataFrame with player names by concatenating firstName and lastName."""
    player_full_name_df = create_player_name_df(master_df)

    """Join DataFrames representing full info by player ID and player full names using a left join."""
    full_info_df = join_and_select_full_info(full_info_by_id_df, player_full_name_df)

    """Get the top 10 players by goals from the provided DataFrame."""
    top_10_goals_df = get_top_10_players_by_goals(full_info_df)
    return top_10_goals_df


def calculate_awards_count(awards_df: DataFrame) -> DataFrame:
    """
    Calculate the count of awards for each player.

    Parameters:
        awards_df (DataFrame): PySpark DataFrame containing awards data.

    Returns:
        DataFrame: PySpark DataFrame with the count of awards for each player.
    """
    awards_count_df = awards_df.groupBy("playerID").agg(count("*").alias("Awards"))
    return awards_count_df


def calculate_goals_count(scoring_df: DataFrame) -> DataFrame:
    """
    Calculate the count of goals for each player.

    Parameters:
        scoring_df (DataFrame): PySpark DataFrame containing scoring data.

    Returns:
        DataFrame: PySpark DataFrame with the count of goals for each player.
    """
    goals_count_df = scoring_df.groupBy("playerID").agg(count("*").alias("Goals"))
    return goals_count_df


def get_distinct_teams(teams_df: DataFrame) -> DataFrame:
    """
    Get distinct teams from the input DataFrame.

    Parameters:
        teams_df (DataFrame): PySpark DataFrame containing teams data.

    Returns:
        DataFrame: PySpark DataFrame with distinct teams.
    """
    distinct_team_name_df = teams_df.select("tmID", "name").distinct()
    return distinct_team_name_df


def join_awards_and_goals(awards_count_df: DataFrame, goals_count_df: DataFrame) -> DataFrame:
    """
    Join DataFrames representing awards count and goals count on playerID using an outer join.

    Parameters:
        awards_count_df (DataFrame): PySpark DataFrame containing awards count data.
        goals_count_df (DataFrame): PySpark DataFrame containing goals count data.

    Returns:
        DataFrame: PySpark DataFrame with awards and goals counts joined on playerID.
    """
    joined_df = awards_count_df.join(goals_count_df, on="playerID", how="outer")
    return joined_df


def get_player_teams(scoring_df: DataFrame, distinct_team_name_df: DataFrame) -> DataFrame:
    """
    Get teams for each player based on scoring data and distinct team names using a left join.

    Parameters:
        scoring_df (DataFrame): PySpark DataFrame containing scoring data.
        distinct_team_name_df (DataFrame): PySpark DataFrame containing distinct team names.

    Returns:
        DataFrame: PySpark DataFrame with teams for each player.
    """
    player_teams_df = (scoring_df.select("playerID", "tmID")
                       .distinct()
                       .join(distinct_team_name_df, on="tmID", how="left")
                       .groupBy("playerID")
                       .agg(collect_list("name").alias("Teams")))
    return player_teams_df


def join_awards_goals_and_teams(awards_goals_by_player_df: DataFrame, player_teams_df: DataFrame) -> DataFrame:
    """
    Join DataFrames representing awards and goals by player and player teams using a left join.

    Parameters:
        awards_goals_by_player_df (DataFrame): PySpark DataFrame containing awards and goals by player data.
        player_teams_df (DataFrame): PySpark DataFrame containing player teams data.

    Returns:
        DataFrame: PySpark DataFrame with awards, goals, and teams joined on playerID.
    """
    joined_df = awards_goals_by_player_df.join(player_teams_df, on="playerID", how="left")
    return joined_df


def create_player_name_df(master_df: DataFrame) -> DataFrame:
    """
    Create a DataFrame with player names by concatenating firstName and lastName.

    Parameters:
        master_df (DataFrame): PySpark DataFrame containing master data.

    Returns:
        DataFrame: PySpark DataFrame with player names and playerID.
    """
    player_name_df = (master_df.withColumn("Name", concat_ws(" ", col("firstName"), col("lastName")))
                      .select("playerID", "Name")
                      .distinct())
    return player_name_df


def join_and_select_full_info(full_info_by_id_df: DataFrame, player_full_name_df: DataFrame) -> DataFrame:
    """
    Join DataFrames representing full info by player ID and player full names using a left join.
    Select columns "Name", "Awards", "Goals", and "Teams".

    Parameters:
        full_info_by_id_df (DataFrame): PySpark DataFrame containing full info by player ID.
        player_full_name_df (DataFrame): PySpark DataFrame containing player full names.

    Returns:
        DataFrame: PySpark DataFrame with selected columns joined on playerID.
    """
    joined_and_selected_df = (full_info_by_id_df.join(player_full_name_df, on="playerID", how="left")
                              .select("Name", "Awards", "Goals", "Teams"))
    return joined_and_selected_df


def get_top_10_players_by_goals(full_info_df: DataFrame) -> DataFrame:
    """
    Get the top 10 players by goals from the provided DataFrame.

    Parameters:
        full_info_df (DataFrame): PySpark DataFrame containing full info.

    Returns:
        DataFrame: PySpark DataFrame with the top 10 players by goals.
    """
    top_10_goals_df = full_info_df.orderBy("Goals", ascending=False).limit(10)
    return top_10_goals_df
