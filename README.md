# PySparkFinalTask

# Requirements:
Download 'Professional Hockey Database' datasets https://www.kaggle.com/open-source-sports/professional-hockey-database
AwardsPlayers.csv
Master.csv
Scoring.csv
Teams.csv
You can find then in source folder.

# Create Spark job:
Count number of awards for each player
Count number of goals (Scoring.stint)
Find in which teams played each player
Limit top 10 players by goals and save in text format (and few other formats of your choice, avro, parquet...)
Write it in 3 methods: RDD, DataFrame API and SQL query

# Description

This module functionality includes 3 implementation: Dataframe API, RDD, SQL
Result will be saved in sink folder with dataframe, rdd, sql sub-folders inside, 
based on ran implementation.

To choose required implementation - put one of arguments [Dataframe, RDD, SQL]
By default application will be process Dataframe API implementation.

# Run instructions:
    # Dataframe
    spark-submit --master local --packages org.apache.spark:spark-avro_2.12:3.5.0 main.py
        
    #RDD
    spark-submit --master local --packages org.apache.spark:spark-avro_2.12:3.5.0 main.py RDD

    #SQL
    spark-submit --master local --packages org.apache.spark:spark-avro_2.12:3.5.0 main.py SQL

You need to use additional package to be able to save in avro format, as from new spark 
versions spark-avro package was excluded.

# Test execution and coverage

    #Test execution
    pytest -v

It should be run from root of application and -v gives you detail information.

    #Test coverage
    coverage run -m unittest discover -s pysparkfinaltask\tests -p 'test_*.py'

    coverage report -m
    or
    coverage html

Coverage should be started from one level outside app directory. After coverage completed,
you will need to create report and you can choose 2 options:
    - default report
    - HTML report

Default report will be shown on Console.
For HTML report will be created "htmlcov\index.html", this link you can put 
directly to command line to open HTML report.