from pyspark.sql import SparkSession


def readCSV(session: SparkSession, path: str):
    return session.read.csv(path, header=True, inferSchema=True)
