from pyspark.sql import DataFrame
from pyspark.sql.functions import concat_ws, col, when


def write_to_text(top_10_goals_df: DataFrame, output_path: str):
    """
    Write the top 10 players' information to a text file.

    Parameters:
        top_10_goals_df (DataFrame): PySpark DataFrame containing top 10 players' information.
        output_path (str): Path where the text file will be saved.
    """
    result_df = (top_10_goals_df.withColumn("Top10",
                                            concat_ws(",", col("Name"),
                                                      when(col("Awards").isNotNull(),
                                                           col("Awards").cast("string")).otherwise("null"),
                                                      when(col("Goals").isNotNull(),
                                                           col("Goals").cast("string")).otherwise("null"),
                                                      col("Teams").cast("string")))
                 .drop("Name", "Awards", "Goals", "Teams"))

    # Write to text file
    result_df.write.mode("overwrite").text(output_path)


def write_to_avro(top_10_goals_df: DataFrame, output_path: str):
    """
    Write the top 10 players' information to an Avro file.

    Parameters:
        top_10_goals_df (DataFrame): PySpark DataFrame containing top 10 players' information.
        output_path (str): Path where the Avro file will be saved.
    """
    top_10_goals_df.write.format("avro").save(output_path, mode="overwrite")


def write_to_parquet(top_10_goals_df: DataFrame, output_path: str):
    """
    Write the top 10 players' information to a Parquet file.

    Parameters:
        top_10_goals_df (DataFrame): PySpark DataFrame containing top 10 players' information.
        output_path (str): Path where the Parquet file will be saved.
    """
    top_10_goals_df.write.parquet(output_path, mode="overwrite")
