from pyspark.sql import SparkSession


class SparkSessionSingleton:
    def __init__(self, app_name="hokey_task",
                 spark_jars_packages="org.apache.spark:spark-avro_2.12:3.5.0",
                 spark_local_dir="temp"):
        self.__spark_session = SparkSession.builder \
            .appName(app_name) \
            .config("spark.jars.packages", spark_jars_packages) \
            .config("spark.local.dir", spark_local_dir) \
            .getOrCreate()

    def get_spark_session(self):
        return self.__spark_session
